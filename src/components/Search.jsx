import React, { Fragment, Component } from 'react';
import { Menu, Icon, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import SearchItem from './SearchItem';
import './styles.less';

function isGroup(item) {
  return item.children && item.children.length > 0;
}

function getDetailText(detailText, item, items) {
  if (_.isFunction(detailText)) {
    return detailText(item, items);
  }
  return item[detailText];
}

const initialState = {
  activeIndex: null,
  isSearching: false,
  filteredItems: [],
  selectedIndex: null,
  query: '',
};

export default class Search extends Component {
  componentWillMount() {
    this.setState(initialState);
  }

  filterItems = (query = '') => {
    const filteredItems = [];
    const { items, mainText } = this.props;
    items.forEach((item) => {
      if (isGroup(item)) return;
      const match = item[mainText].toLowerCase().indexOf(query.toLowerCase()) >= 0;
      if (match) {
        filteredItems.push(item);
      }
    });
    return filteredItems;
  }

  selectItem = (index) => {
    const item = this.state.filteredItems[index];
    this.props.onSelectItem(item);
    this.setState({
      selectedIndex: index,
      activeIndex: index,
    });
  }

  handleOnChangeSearch = (e, data) => {
    const query = data.value;
    const isSearching = query.length > 0;
    if (this.props.onSearch) {
      this.props.onSearch(isSearching, query);
    }
    let filteredItems = [];
    if (isSearching) {
      filteredItems = this.filterItems(query);
    }

    this.setState({
      activeIndex: isSearching ? 0 : null,
      isSearching,
      filteredItems,
      query,
    });
  }

  handleOnKeyDownSearch = (e) => {
    let { activeIndex } = this.state;

    if (e.keyCode === 13) {
      this.selectItem(activeIndex);
      return;
    }

    switch (e.keyCode) {
      case 40: activeIndex += 1; break;
      case 38: activeIndex -= 1; break;
      default: return;
    }

    e.preventDefault();

    activeIndex = Math.max(activeIndex, 0);
    activeIndex = Math.min(activeIndex, this.state.filteredItems.length - 1);
    this.setState({
      activeIndex,
    });
  }

  handleItemClick = index => () => this.selectItem(index)

  reset = () => {
    this.setState(initialState);
  }

  render() {
    const { iconOnly, mainText, items, detailText } = this.props;
    const { selectedIndex, activeIndex, query, isSearching, filteredItems } = this.state;
    return (
      <div className="mf-search">
        <Menu.Item className="search-input">
          {iconOnly
            ? <Icon name="search" />
            : <Input
              input={<input onKeyDown={this.handleOnKeyDownSearch} />}
              onChange={this.handleOnChangeSearch}
              icon="search"
              placeholder="Cerca..."
              transparent
              inverted
            />
          }
        </Menu.Item>
        {isSearching ?
          <Menu.Menu className="search-results">
            {filteredItems.map((item, itemIndex) => (
              <SearchItem
                mainText={item[mainText]}
                detailText={getDetailText(detailText, item, items)}
                icon={item.icon}
                query={query}
                key={item.id}
                iconOnly={iconOnly}
                onClick={this.handleItemClick(itemIndex)}
                active={itemIndex === activeIndex}
                selected={itemIndex === selectedIndex}
              />
            ))}
          </Menu.Menu> : ''}
      </div>
    );
  }
}

Search.defaultProps = {
  iconOnly: false,
  detailText: null,
  onSearch: null,
};

Search.propTypes = {
  iconOnly: PropTypes.bool,
  onSearch: PropTypes.func,
  detailText: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  onSelectItem: PropTypes.func.isRequired,
  mainText: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
};
