import React, { Fragment } from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const QueryElement = ({ mainText, detailText, query, active }) => {
  const start = mainText.toLowerCase().indexOf(query.toLowerCase());
  const end = start + query.length;

  const pre = mainText.substring(0, start);
  const found = mainText.substring(start, end);
  const after = mainText.substring(end, mainText.length);
  return (
    <Fragment>
      {pre}
      <span className="highlightQuery">{found}</span>
      {after} {detailText.length > 0 ? '-' : ''} <small>{detailText}</small>
      {active ? (<strong className="enter">invio</strong>) : ''}
    </Fragment>
  );
};

QueryElement.propTypes = {
  mainText: PropTypes.string.isRequired,
  query: PropTypes.string.isRequired,
  detailText: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
};

const SearchItem = ({ icon, mainText, detailText, active, selected, query, onClick, iconOnly }) => (
  <Menu.Item as="a" active={active || selected} onClick={onClick} >
    {icon && icon.length > 0 ? <Icon name={icon} /> : ''}
    <span>
      {!iconOnly ?
        <QueryElement
          mainText={mainText}
          detailText={detailText}
          query={query}
          active={active}
        />
        : ''}
    </span>
  </Menu.Item>
);

SearchItem.defaultProps = {
  icon: '',
  detailText: '',
};

SearchItem.propTypes = {
  icon: PropTypes.string,
  mainText: PropTypes.string.isRequired,
  detailText: PropTypes.string,
  active: PropTypes.bool.isRequired,
  selected: PropTypes.bool.isRequired,
  query: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  iconOnly: PropTypes.bool.isRequired,
};

export default SearchItem;
