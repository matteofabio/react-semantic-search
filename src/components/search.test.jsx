import React from 'react';
import { render } from 'react-dom';
import Search from './Search';

const items = [
  {
    id: 0,
    title: 'Ordini',
    icon: 'cart',
    to: '/OrdersList',
  },
  {
    id: 1,
    title: 'Albero Genealogico Agenti',
    icon: 'tree',
    to: '/TreeAgents',
  },
  {
    id: 2,
    title: 'Dati Agente',
    icon: 'address card outline',
    to: '/privacy/:id',
  },
];


const Example = () => (
  <Search
    items={items}
    onSelectItem={() => 1}
    mainText="title"
  />
);

it('Search renders without crashing', () => {
  const div = document.createElement('div');
  render(<Example />, div);
});
