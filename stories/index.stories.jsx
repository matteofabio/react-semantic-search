import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Sidebar, Menu } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.css';
import Search from '../src';

const items = [
  {
    id: 0,
    title: 'Ordini',
    icon: 'cart',
    to: '/OrdersList',
    children: [3, 4]
  },
  {
    id: 1,
    title: 'Albero Genealogico Agente',
    icon: 'tree',
    to: '/TreeAgents',
  },
  {
    id: 2,
    title: 'Dati Agente',
    icon: 'address card outline',
    to: '/privacy/:id',
  },
  {
    id: 3,
    title: 'Aggiungi',
    icon: 'plus',
    to: '/TreeAgents',
  },
  {
    id: 4,
    title: 'Rimuovi',
    icon: 'minus',
    to: '/privacy/:id',
  },
];

storiesOf('Search', module)
  .add('sidebar search', () => (
    <Sidebar as={Menu} animation="overlay" visible vertical inverted>
      <Search
        items={items}
        onSelectItem={() => console.log(1)}
        mainText="title"
      />
    </Sidebar>
  ));
