# Search items in an array

## Install
```
npm i -S @matteofabio/react-semantic-search
```

## Example
```jsx
import Search from '@matteofabio/react-semantic-search'
import React from 'react';
import { Sidebar, Menu } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.css'

const items = [
  {
    id: 0,
    title: 'Ordini',
    icon: 'cart',
    to: '/OrdersList',
  },
  {
    id: 1,
    title: 'Albero Genealogico Agenti',
    icon: 'tree',
    to: '/TreeAgents',
  },
  {
    id: 2,
    title: 'Dati Agente',
    icon: 'address card outline',
    to: '/privacy/:id',
  },
];
return (
    <Sidebar as={Menu} animation="overlay" visible vertical inverted>
        <Search
        items={items}
        onSelectItem={(item) => console.log(item)}
        mainText="title"
        />
    </Sidebar>
)
```

## Getting Started

Install dependencies

`npm install`

Start storybook

`npm run storybook`

## Testing

`npm run test`

## Build library

`npm run build`

Produces production version of library under the `build` folder.

## Publish library

`npm publish`